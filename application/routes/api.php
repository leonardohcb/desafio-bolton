<?php

use Illuminate\Http\Request;
use App\Http\Resources\Document as DocumentResource;
use App\Document;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::get('/document/{accessKey}', function ($accessKey) {
        return new DocumentResource(Document::whereAccessKey($accessKey)->firstOrFail());
    });
});

Route::fallback(function(){
    return response()->json([
        'code' => 404,
        'message' => 'Página não encontrada. Verifique a URL e se o erro persistir entre em contato com leonardohcb@gmail.com'
    ], 404);
});