<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\SandboxHandler;
use App\Helpers\NfeXmlParser;
use App\Document;
use App\Exceptions\NFeXmlNotFoundException;
use App\Exceptions\NFeTotalValueNotFoundException;

class FetchDocuments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'documents:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch Arquivei sandbox to populate DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            $sandboxHandler = new SandboxHandler();
            $uri = null;
            $page = 1;
            do {
                $this->comment("Buscando documentos na API (página {$page})");
                $response = $sandboxHandler->get($uri);
                $this->info("Foram encontrados {$response->count} documentos");

                $bar = $this->output->createProgressBar($response->count);
                $bar->start();
                foreach ($response->data as $document) {
                    self::processDocument($document);
                    $bar->advance();
                }
                $bar->finish();
                $this->line("\n");
                
                $uri = $response->page->next;
                $page++;
            } while ($response->count == 50);

            $this->comment("Todas as páginas foram processadas. Finalizando");

            return true;
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    private function processDocument($document)
    {
        try {
            Document::firstOrCreate([
                'access_key' => $document->access_key,
                'value' => NfeXmlParser::loadFromBase64($document->xml)->getVNf(),
            ]);
        } catch (NFeXmlNotFoundException $e) {
            $this->error("Não foi possível identificar estrutura XML de NFe no documento de access_key: {$document->access_key}");
        } catch (NFeTotalValueNotFoundException $e) {
            $this->error("Valor total da NFe (infNFe > total > ICMStot > vNF) não encontrado no documento de access_key: {$document->access_key}");
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
