<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
	const MODEL_NOT_FOUND_EXCEPTION_MESSAGE = 'Documento não encontrado para chave de acesso informada.';
	
    protected $fillable = ['access_key', 'value'];
}
