<?php

namespace App\Helpers;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class SandboxHandler
{
    private $client;
    private $headers;

    public function __construct()
    {
        if (!config('sandbox.api_id') || !config('sandbox.api_key')) {
            throw new \Exception ("API da Arquivei não configurada. Verifique os valores de SANDBOX_API_ID e SANDBOX_API_KEY no arquivo .env e execute novamente.");
        }

        $this->client = new Client();
        $this->headers = [
            'x-api-id' => config('sandbox.api_id'),
            'x-api-key' => config('sandbox.api_key'),
            'Content-Type' => 'application/json',
        ];
    }

    public function get($uri = null)
    {
        $uri = $uri ?? config('sandbox.base_uri');
        try {
            $result = $this->client->request('GET', $uri, [
                'headers' => $this->headers,
            ]);

            return json_decode($result->getBody()->getContents());

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $jsonResponse = json_decode($e->getResponse()->getBody()->getContents());
            throw new \Exception("Não foi possível obter resposta da API da Arquivei. (Erro {$jsonResponse->status->code}: {$jsonResponse->status->message}). Verifique as configurações no arquivo .env");
            
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
