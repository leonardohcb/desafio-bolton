<?php

namespace App\Helpers;

use App\Exceptions\NFeXmlNotFoundException;
use App\Exceptions\NFeTotalValueNotFoundException;

class NfeXmlParser
{
    private $nfe;

    public static function loadFromBase64($base64Xml)
    {
        $xml = \XmlParser::extract(base64_decode($base64Xml))->getContent();
        $obj = new self();
        $obj->nfe = $xml->NFe ? $xml->NFe->infNFe : $xml->infNFe;
        if (!$obj->nfe) {
            throw new NFeXmlNotFoundException();
        }
        return $obj;
    }

    public function getVNf()
    {
        if (!isset($this->nfe->total) || !isset($this->nfe->total->ICMSTot) || !isset($this->nfe->total->ICMSTot->vNF)) {
            throw new NFeTotalValueNotFoundException();
        }
        return $this->nfe->total->ICMSTot->vNF;
    }
}
