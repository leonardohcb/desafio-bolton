<?php

namespace Tests\Unit;

use App\Helpers\SandboxHandler;
use Tests\TestCase;

class SandboxHandlerTest extends TestCase
{
    /**
     * Test sandbox access with given credentials (.env)
     *
     * @return void
     */
    public function testSandboxAccess()
    {
    	$sandbox = new SandboxHandler();
    	$response = $sandbox->get();
    	$this->assertEquals($response->status->code, 200);
    }
}
