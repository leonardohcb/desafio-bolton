<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Document;
use App\Http\Resources\Document as DocumentResource;
use Tests\TestCase;


class DocumentTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test new document creation.
     *
     * @return void
     */
    public function testCreate()
    {
        $document = factory(Document::class)->create();
        $this->assertDatabaseHas('documents', ['access_key' => $document->access_key, 'value' => $document->value]);
    }

    /**
     * Test document search by access_key.
     *
     * @return void
     */
    public function testShow()
    {
        $document = factory(Document::class)->create();
        $returnedDocument = Document::whereAccessKey($document->access_key)->first();
        $this->assertEquals($returnedDocument->value, $document->value);
    }

    /**
     * Test document resource response.
     *
     * @return void
     */
    public function testResource()
    {
        $document = factory(Document::class)->create();
        $resource = (new DocumentResource($document))->response()->getData(true);
        $this->assertEquals($resource, ['code' => 200, 'data' => ['value' => $document->value]]);
    }
}
