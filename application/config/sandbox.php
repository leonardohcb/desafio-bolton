<?php

return [
	"base_uri" => "https://sandbox-api.arquivei.com.br/v1/nfe/received",
	"api_id" => env('SANDBOX_API_ID', null),
	"api_key" => env('SANDBOX_API_KEY', null),
];