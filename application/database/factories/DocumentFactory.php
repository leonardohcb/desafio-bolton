<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Document;
use Faker\Generator as Faker;

$factory->define(Document::class, function (Faker $faker) {
    return [
        'access_key' => $faker->regexify('[0-9]{44}'),
        'value' => $faker->randomFloat(2, 0.01, 999999999.99),
    ];
});
