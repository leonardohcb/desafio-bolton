
# Desafio Bolton

## Instalação

Clonar o repositório:

     git clone https://bitbucket.org/leonardohcb/desafio-bolton.git

Acessar a pasta do repositório e ajustar permissões:

     cd desafio-bolton
     sudo chmod 777 -R application/storage/
     sudo chmod 777 -R application/bootstrap/cache/

Montar imagens do Docker:

     make build

Iniciar os containers do Docker:

     make start

> O Banco de Dados e o Hostname serão criados com base nas variáveis presentes no arquivo `docker_bolton/docker-compose.yml`
>
> Valores padrão:
>
>      POSTGRES_DB: bolton
>      POSTGRES_USER: bolton
>      POSTGRES_PASSWORD: q1w2e3
>
> Essas variáveis podem ser alteradas para qualquer valor **antes de rodar o comando** `make start` pela primeira vez


Rodar script de instalação:

     make create-project

O arquivo `application/.env` será criado ao final do script.
Através de seu editor de texto favorito, edite `application/.env` atualizando as seguintes variáveis de ambiente de acordo com o conteúdo do arquivo `docker_bolton/docker-compose.yml`:

     DB_DATABASE - com o mesmo valor de POSTGRES_DB
     DB_USERNAME - com o mesmo valor de POSTGRES_USER
     DB_PASSWORD - com o mesmo valor de POSTGRES_PASSWORD

Também será necessário informar os valores de `SANDBOX_API_ID` e `SANDBOX_API_KEY` de acordo com suas credenciais de acesso a API da Arquivei

Criar tabelas no Banco de Dados:

     make artisan migrate

Está pronto para usar!

### Executando
Após instalado, para futuras execuções basta rodar:

     make start

## Usando
### Buscando documentos de integração com API
Para executar a integração com a API execute

     make artisan documents:fetch
Será executada a integração página por página até percorrer todos os resultados

### Pesquisa por chave de acesso
Através de um browser ou pelo Postman execute uma chamada GET para

     http://localhost/api/v1/document/<chave_de_acesso>

substituindo <chave_de_acesso> pela chave desejada. Exemplo:

     http://localhost/api/v1/document/35140330290824000104550010003715421390782397

No caso de **sucesso**, ou seja, a chave foi integrada e existe no Banco de Dados, será retornado um JSON no seguinte formato:

     {
          "data": {
               "value": <valor_total_do_documento>,
          },
          "code": 200
      }
onde <valor_total_do_documento> conterá o valor da tag vNF do documento. Exemplo:

     {
          "data": {
               "value": 365.89
          },
          "code": 200
     }

> No caso de **erro**, ou seja, da chave de acesso **não ser encontrada**, será retornado o seguinte JSON:
>
>      {
>           "code": 404,
>           "message": "Documento não encontrado para chave de acesso informada."
>      }

## Executando testes unitários
O projeto possui testes unitários para validar:

 - inserção e seleção dos documentos no Banco de dados
 - formato de retorno do documento com o valor total
 - acesso a sandbox utilizando as credenciais do arquivo `application/.env`

Para executar os testes:

     make phpunit

ou, para maiores detalhes:

     make phpunit-debug

## Desligando o projeto
Para desligar o docker e remover os containers:

     make stop
