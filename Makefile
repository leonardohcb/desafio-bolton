# Makefile for Docker

DIR = 'docker_bolton'

help:
	@echo ""
	@echo "usage: make COMMAND"
	@echo ""
	@echo "Commands:"
	@echo "  build               Build images"
	@echo "  rebuild-images      Build images ignoring cache"
	@echo "  start               Create and start containers"
	@echo "  create-project      -- Create and start containers"
	@echo "  composer            -- Create and start containers"
	@echo "  stop                Stop all services"
	@echo "  logs                Follow log output"
	@echo "  php                 Open php container"
	@echo "  artisan             Run artisan commands as host user"
	@echo "  phpunit             Run unit tests"
	@echo "  phpunit-debug       Run unit tests with --debug information"

build:
	@cd $(DIR) && docker-compose build

rebuild-images:
	@cd $(DIR) && docker-compose build --no-cache

start:
	@cd $(DIR) && docker-compose up -d

create-project:
	@cd $(DIR) && docker exec -u $(shell id -u):$(shell id -g) -it $(shell cd $(DIR) && docker-compose ps -q php) composer create-project

composer:
	@cd $(DIR) && docker exec -u $(shell id -u):$(shell id -g) -it $(shell cd $(DIR) && docker-compose ps -q php) composer $(filter-out $@,$(MAKECMDGOALS))

stop:
	@cd $(DIR) && docker-compose down -v

logs:
	@cd $(DIR) && docker-compose logs -f

php:
	@cd $(DIR) && docker exec -it $(shell cd $(DIR) && docker-compose ps -q php) bash

artisan:
	@cd $(DIR) && docker exec -u $(shell id -u):$(shell id -g) -it $(shell cd $(DIR) && docker-compose ps -q php) php artisan $(filter-out $@,$(MAKECMDGOALS))

phpunit:
	@cd $(DIR) && docker exec -u $(shell id -u):$(shell id -g) -it $(shell cd $(DIR) && docker-compose ps -q php) bash -c "php artisan config:clear && vendor/bin/phpunit"

phpunit-debug:
	@cd $(DIR) && docker exec -u $(shell id -u):$(shell id -g) -it $(shell cd $(DIR) && docker-compose ps -q php) bash -c "php artisan config:clear && vendor/bin/phpunit --debug"

%:
	@: